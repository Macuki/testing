﻿/* Лекция 1
#include <iostream> // подключается библиотека ввода-вывода

int main() // функция main называетя точкой входа в программу(именно с нее начинается выполнение программы)
{
	setlocale(LC_ALL,"Rus"); // - библиотечная функция,которая устанавливает русскую локализацию(в данной ситуации)
	std::cout << "Привет, мир\n"; // cout - вывод, std - standart functions(стандартные функции), \n - перевод на новую строку

	// программа сложения двух чисел
	 signed char a, b; // - Переменные - именованный участок памяти
	// Типы данных

	//int - Целочисленный тип, 4 байта, -2^31 .. 2^31-1
	//char - симольный тип в кодировке ASCII, 1 байт -128 .. 127
	//short - целочисленный тип  , 2 байта, -32768 .. 32767
	//unsigned int Целочисленный тип, 4 байта, 0 .. 2^32 -1
	//unsigned char - символьный тип  1 байт, 0 .. 255
	//unsigned short - Целочисленный тип, 2 байта , 0 .. 65535
	//long long - Целочисленный тип, 8 байт , -2^63 .. 2^63 - 1
	//unsigned long long  целочисленный тип , 8 байта , 0 .. 2^64 - 1
	//float - вещественный тип, 4 байта, содержит до 7 значащих цифр
	//double -  вещественный тип , 8 байт, содержит до 14 значащих цифр



	//  std::cin >> a >> b; // Считывает данные  с клавиатуры ( два числа)
	// signed char c = a + b;

	//Арифметические операции:
	//+, - , * , /(целочисленное деление для целого типа, для вещественных - обычное деление из математики) , % ( остаток от деления, только для целочисленных типов)
	//pow(a,b) - функция, которая возводит "a" в степень "b"
	//sqrt(a) - квадратный корень из а
	//round(a) - обычное округление
	//floor(a) -округление в нижнюю сторону

	std::cout << c << std::endl;



	return 0;
}
*/
/* Лабораторная работа номер 1:
Задание 1)Написать программу, которая на вход получает 3 целых числа A,B,C и рассчитать площадь по формуле Герона
#include <iostream>
int main()
{
	float A, B, C;
	float S, p;
	std::cin >> A >> B >> C;
	p = (A + B + C) / 2;
	S = sqrt((p)*(p-A)*(p-B)*(p-C));
	std::cout << S << std::endl;

	return 0;
}
*/
/* Лекция 2
#include <iostream>

using std::cin; // Можно теперь вместо std :: cin писать просто cin
using std::cout; // Можно теперь вместо std :: cout писать просто cout
using std::endl; // Можно теперь вместо std :: cin писать просто endl
int main()
{
	setlocale(LC_ALL, "Rus");

	int x;
	cin >> x;
	// Операторы сравнения
	// > ( Больше), < (меньше) , >= (больше или равно),
	// <=(меньше или равно), == (равно), != (не равно)

	if (x > 0)
	{
		cout << "Положительное" << endl;
		if (x > 50)
		{
			cout << "X больше 50\n";
		}
		else
		{
			cout << "X меньше 50\n";
		}
	}
	else if (x < 0)
	{
		cout << "Отрицательное" << endl;

	}
	else
	{
		cout << " Равно 0" << endl;
	}

	// Проверить, входит ли x в промежуток от 0 до 100

	if (x > 0)
	{
		if (x < 100)
			cout << "Входит" << endl;
		else
			cout << "Не входит" << endl;
	}
	else
		cout << "Не входит" << endl;
	//Тоже самое, но короче:

	if (x > 0 && x < 100)
		cout << "X входит в промежуток\n";
	else
		cout << "Не входит\n";

	// Логические операторы
	// && - И, || - ИЛИ, ! - НЕ
	if (x == 10 || x == 20)
		cout << x << endl;

	// Вывод месяца
	if (x == 1)
		cout << "Январь\n";
	else if (x == 2)
		cout << "Февраль\n";
	else
		cout << "Декабрь\n";
	// Тоже самое, но по-другому:


	switch (x)
	{
		case 1:
			cout << "Январь" << endl;
			break;
		case 2:
			cout << "Февраль" << endl;
			break;
		case 12:
			cout << "Декабрь" << endl;
			break;
		default:
			cout << "Такой цифры нет!\n";
	}

	// тернарный оператор
	//int y = (условие) ? a : b;
	// Если условие выполнено, то y=a, иначе y=b
	// Проверка на четность:
	if (x % 2 == 0)
	{
		cout << "Четное\n";
	}
	return 0;
}
*/
/* Лабораторная работа номер 2:
 Задание 1)Ввести натуральные числа A, B и C. Если А кратно C и B кратно C, вывести (A+B)/C,
 Если A кратно C и B не кратно C, вывести (A/C)+B, в остальных случаях вывести A-B-C.
#include <iostream>
using std::cin;
using std::cout;
int main()
{
	int A, B, C;
	cin >> A >> B >> C;
	if ((A % C == 0) && (B % C == 0))
	{
		cout << (A + B) / C << std::endl;
	}
	else if ((A % C == 0) && (B % C != 0))
	{
		cout << (A / C) + B << std::endl;
	}
	else
	{
		cout << A - B - C << std::endl;
	}
	return 0;
}
 Задание 2)Ввести цифру N, при помощи оператора switch вывести название цифры.
 Предусмотреть обработку ошибочного ввода N.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
int main()
{
	setlocale(LC_ALL, "Rus");
	short N;
	cin >> N;
	switch (N)
	{
		case 0:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 1:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 2:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 3:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 4:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 5:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 6:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 7:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 8:
			cout << "Вы ввели цифру " << N << endl;
			break;
		case 9:
			cout << "Вы ввели цифру " << N << endl;
			break;
		default:
			cout << "Вы ввели неверное значение\n";
	}
	return 0;
}
*/
/* Лекция 3
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
int main()
{
	setlocale(LC_ALL, "Rus");
	// Вывести числа от 0 до 9
	int i = 0;
	// while() {} - Цикл, который дейтсвует по принципу: Пока выполнено (), делай {}
	// i - итератор (счетчик)
	while (i < 10)
	{
		cout << i << endl;
		i++;
	}

	// цикл do .. while(сначала выполнится тело цикла, затем проверится условие, по принципу while)
	int i = 0;
	do
	{
		cout << i << endl;
		i++;
	} while (i < 10);

	// for(a1;a2;a3) {} - цикл, где {} - тело цикла, a1 - стартовое значение(инициализация),
	// a2 - проверка условия, a3 - шаг цикла(Сначала происходит инициализцаия,
	// затем проверяется условие, если выполнено, то выполняется тело цикла,
	// после завершения тела цикла выполняется шаг цикла, затем снова проверяется
	// условие, выполняется тело цикла и так до тех пор, пока условие не станет ложным,
	// иначе просто выходим из цикла)
	 for (int i = 0; i < 10; i++)
		cout << i << endl;

	// i++(пост инкремент). Сначала завершится операция, затем произойдет увеличение
	//значения переменной i на 1

	int i = 0;
	int x = i++; // - Сначала присвоится значение переменной i к иксу, затем произойдет
				 // увеличение переменной i на 1, в итоге программа выведет x = 0, i = 1
	cout << "x = " << x << " i = " << i << endl;

	//++i(пре инкремент). Сначала увеличит значение переменной i на 1, затем
	//выполнит нужную операцию.

	int i = 0;
	int x = ++i; // - сначала значение переменной i увеличится на 1, затем значение
				// переменной i присвоится к иксу.
	cout << "x = " << x << " i = " << i << endl;

	// Поиск Максимума в последовательности
	// дается 10 целых чисел в диапозоне -1000, до 1000

	short max = -1001;
	short y;
	for (int i = 0; i < 10; ++i)
	{
		cin >> y;
		if ((y >= -1000) && (y <= 1000) && (y > max ))
			max = y;

	}
	if (max == -1001)
		cout << "Не удалось найти такого числа среди заданного диапозона\n";
	else
		cout << "Максимум равен " << max << endl;

	//  Область видимости переменной

	int i = 20;
	for ( i ; i < 10; i++)
	{
		cout << "In cycle " << i << endl;
	}
	 cout << "Out cycle" <<  i << endl;

	// Распаковка натурального числа
	// 1. Вывести все его цифры
	// 2. Вывести сумму цифр

	int S = 0;
	int x;
	cin >> x;
	while (x > 0)
	{
		S += x % 10;
		cout << x % 10 << endl;
		x /= 10;
	}
	cout << S << endl;

	// Ввести с клавиатуры N чисел, остановить цикл, при вводе 0

	int N;
	cin >> N;
	int x;
	while (N > 0)
	{
		cin >> x;
		if (x == 0)
		{
			cout << "Get the digital " << x << endl;
			break;
		}
		else
			cout << "Get the digital " << x << endl;
		--N;
	}

	// Определение простоты числа

	int x;
	cin >> x;
	bool flag = true;
	for (int d = 2; d <= sqrt(x); ++d)
	{

		if (x % d == 0)
		{
			flag = false;
			break;
		}

	}
	if (flag == true)
		cout << "Простое\n";
	else
		cout << "Составное\n";

	return 0;
}
*/
/*Лабораторная работа номер 3:
Задание 1)Дана последовательность целых чисел {Aj}. Hайти произведение чисел,
заканчивающихся цифpой 2, наибольшее из таких чисел и номеp этого числа в последовательности.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
int main()
{
	int N,number = 0,x,P=1,max=INT_MIN;
	cin >> N;
	for (int i = 0; i < N; i++)
	{
		cin >> x;
		if ((x % 10 == 2) || (x % 10 == -2))
			{
			P *= x;
			if (x > max)
			{
				max = x;
				number = i + 1;
			}
		}
		else
			continue;
	}
	cout << P << " "<< max <<" " <<  number << endl;
	return 0;
}
*/
/* Лекция 4

#include <iostream>
#include <fstream>
#define N 100 // Определить константу N равной 100
#define M 100
using std::cin;
using std::cout;
using std::endl;
int main()
{
	setlocale(LC_ALL, "Rus");

	//std::ifstream in("input.txt");
	//std::ofstream out("output.txt");

	int n;
	in >> n; // указатель на открытый файл, std:: cin
	for (int i = 0; i < n; i++)
	{
		int x;
		in >> x;
		int y = x * x;
		cout << y << endl;
		out << y << endl;
	}

	// Массивы
	// Фиксированный массив, память выделяется на этапе компиляцию
	//const int N_MAX = 100; // const - значит, что переменная никогда не поменяется

	int n;
	in >> n;
	int mas[N];
	for (int i = 0; i < n; i++)
		in >> mas[i];
	// обработка массива
	// Сортировка обменами ;
	int a = 0;
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
		{
			if (mas[i] < mas[j]) // > по возрастанию, < по убыванию.
			{
				int a = mas[i];
				mas[i] = mas[j];
				mas[j] = a;
			}

		}

	for (int i = 0; i < n; i++) // Range check error( выход за границу)
	{
		out << mas[i] << " ";
	}

	// Двумерный массив

	int matrix[N][M];
	int n, m;
	in >> n >> m;
	// Считывание матрицы
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			in >> matrix[i][j];
	// Обработка матрицы - поиск максимального элемента матрицы
	int max = matrix[0][0];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			if (max < matrix[i][j])
				max = matrix[i][j];
	//Вывод
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			out << matrix[i][j] << " ";
		out << endl;
	}
	out << "Максимальный элемент = " << max;

	// Строки

	char symbol = 'a';
	cout << (int)symbol << endl; // (int) - привидение к типу int
	char s[100] = "Hello, world";
	cout << s << endl;
	cout << s[1] << endl;
	for (int i = 0; i < 20; i++)
		cout << (int)s[i] << " ";

	// Ввод
	std::ifstream in("inputt.txt");
	std::ofstream out("outputt.txt");
	if (!in)
		cout << "File open error" << endl;
	char str[100];
	in >> str;
	cout << str << endl;
	out << str << endl;
	return 0;
}
*/
/* Лабораторная работа номер 4:
Задание 1) Дана последовательность натуральных чисел {aj}j=1...n (n<=10000). Если в последовательности
есть хотя бы одно простое число, упорядочить последовательность по неубыванию.
#include <iostream>
#include <fstream>
#define N 10000

using std::cin;
using std::cout;
using std::endl;
int main()
{
	std::ifstream in("dz4.1.txt");
	int n;
	in >> n;
	int a[N];
	int d;
	bool flag ;
	for (int i = 0; i < n; i++) // Заполняем массив числами(Вводим последовательность)
		in >> a[i];
	for (int i = 0; i < n; i++) // Проверяем, есть ли простые числа.
	{
		flag = false;
		if (a[i] == 1)
			continue;
		for (int j = 2; j <= sqrt(a[i]);j++)
			if (a[i] % j == 0)
			{
				flag = true;
				break;
			}
		if (flag == false)
			break;
	}
	if (flag == false) // Если флаг равен false, то есть простые числа
					   // в последовательности, => меняем порядок на возрастающий
	{
		for (int i = 0; i < n-1 ; i++)
			for (int j = i + 1; j < n; j++)

				if (a[i] > a[j])
				{
					d = a[i];
					a[i] = a[j];
					a[j] = d;
				}

	}
	for (int i = 0; i < n; i++) // Выводим саму последовательность
		cout << a[i] << " ";
	return 0;
}
Задание 3)Дана целочисленная матрица {Aij}i=1..n,j=1..m (n,m<=100). Найти строку, в
которой меньше всего четных чисел, и заменить все элементы этой строки их квадратами.

#include <iostream>
#include <fstream>
#define N 100
#define M 100
using std::cin;
using std::cout;
using std::endl;

int main()
{
	std::ifstream in("dz4.3.txt");
	int n, s,min = INT_MAX,currently,number;
	in >> n >> s;
	int a[N][M];
	for (int i = 0; i < n; i++)
	{
		currently = 0;
		for (int j = 0; j < s; j++)
		{
			in >> a[i][j];
			if (a[i][j] % 2 == 0)
				currently += 1;
		}
		if ((currently < min) && (currently != 0))
		{
			min = currently;
			number = i;
		}

	}
	if (min != INT_MAX)
		for (int j = 0; j < s; j++)
		{
			a[number][j] = a[number][j] * a[number][j];
		}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < s; j++)
			cout << a[i][j] << " ";
		cout << endl;
	}
	return 0;
}
Задание 2) Ввести последовательность натуральных чисел {Aj}j=1...n (n<=1000). Упорядочить
последовательность по неубыванию произведения цифр числа, числа с одинаковыми произведениями цифр
дополнительно упорядочить по неубыванию первой цифры числа, числа с одинаковыми произведениями цифр и
одинаковыми первыми цифрами дополнительно упорядочить по неубыванию самого числа
#include <iostream>
#include <fstream>
#define N 100
int pr(int a)
{
	int x=1;
	while (a > 0)
	{
		x *= (a % 10);
		a = a / 10;
	}
	return x;
}

int first(int a)
{
	int x = 0;
	while (a >= 10)
		a /= 10;
	x = a;
	return x;
}
int main()
{

	std::ifstream in("dz4.2.txt");
	int n,tmp;
	int a[N];
	in >> n;
	for (int i = 0; i < n; ++i)
		in >> a[i];
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (pr(a[i]) > pr(a[j]))
			{
				tmp = a[i];
				a[i] = a[j];
				a[j] = tmp;
			}
			else if (pr(a[i]) == pr(a[j]))
				if (first(a[i]) > first(a[j]))
				{
					tmp = a[i];
					a[i] = a[j];
					a[j] = tmp;
				}
				else if (first(a[i]) == first(a[j]))
					if (a[i] > a[j])
					{
						tmp = a[i];
						a[i] = a[j];
						a[j] = tmp;
					}
	for (int i = 0; i < n; ++i)
		std::cout << a[i] << " ";
	return 0;
}
*/
/* Лекция 5
#include <iostream>
#include<fstream>
#include<windows.h>
using std::cin;
using std::cout;
using std::endl;
int main()
{
	setlocale(LC_ALL, "Rus");
	SetConsoleCP(1251);

	std::ifstream in("inputt.txt");
	std::ofstream out("outputt.txt");
	// Символ

	char c,t;
	in >> c >> t;
	cout << c <<" " << t << endl;
	out << c;

	// Посимвольный ввод

	char c;
	while (in >> c) // пока можно прочитать
	{
		cout << c;
	}

	// Найти наиболее часто встречающийся символ в тексте.
	// Верхний регистр ПРТС
	// Нижний регистр пртс

	int symbols[256];
	for (int i = 0; i < 256; i++)
		symbols[i] = 0;
	unsigned char c;
	while (in >> c) // пока можно прочитать
	{	// Заглавные буквы
		if (c >= 'A' && c <= 'Z')
			c += 32;
		symbols[c]++;
		//cout << c << " " << (int)c << endl;;
	}
	for (unsigned char i = 'd'; i < 130; i++)
		cout << symbols[i] << " " << char(i) <<  endl;

	// Строка

	char s[100],t[100];
	in >> s >> t;
	cout << s <<" " <<  t;

	// Ввод по словам

	char s[100];
	while (!in.eof())
	{
		in >> s;
		cout << " < " << s << " > "<< endl;
	}

	// Ввод построчный

	char s[100];
	while (!in.eof())
	{
		in.getline(s, 100);
		//cout << s << endl;
		// Проверка на наличие T в строке

		for (int i = 0; i < strlen(s); i++)
			if (s[i] == 'T')
				cout << "Есть T" << endl;

		// замена строчных букв на заглавные
		for (int i = 0; i < strlen(s); i++)
			if (s[i] >= 'a' && s[i] <= 'z')
				s[i] -= 32;
		cout << s << endl;;
	}


	//Объединение строк

	char s1[100] = "Hello, ";
	char s2[100] = "World!";
	strcat_s(s1,s2);
	cout << s1 << " " << s2 << endl;

	// сравнение строк

	char s3[100] = "aaca";
	char s4[100] = "aac";
	cout << strcmp(s3, s4) << endl;

	// Копирование строк

	char s1[100] = "";
	char s2[100] = "Hello World";
	strcpy_s(s1,s2);
	cout << s1 << " | " << s2 << endl;

	// C++
	std::string s = "Hello, world";
	cout << s << " " << s.length()<<" " << s[4] << endl;
	for (int i = 0; i < s.length(); i++)
		if (s[i] >= 'a' && s[i] <= 'z')
			s[i] -= 32;
	cout << s << endl;
	// Сравнение строк
	std::string s1 = "acb";
	std::string s2 = "abb";
	if (s1 < s2)
	{
		std::cout << s1 << " less " << s2 << endl;
	}
	else if (s1 == s2)
		std::cout << s1 << " equally " << s2 << endl;
	else
		std::cout << s1 << " more " << s2 << endl;
	// Сложение строк
	cout << s1 + s2 << endl;
	// Найти элемент в строке
	// cout << s.find('O') << endl;

	for (int i = 0; i < s.length(); i++)
		if (s[i] == 'O')
			cout << "Found O" << i << endl;

	return 0;
}
*/
/* Лабораторная работа номер 5:
Задание 1)Дана строка длиной не более 100 символов. Посчитать количество букв в ней.
#include <iostream>
#include <fstream>
using std::cin;
using std::cout;
using std::endl;
int main()
{
	char s[100] = "kaofekfoekk12421403k214210ek1e12ke12e21e21ek1";
	int c=0;
	for (int i = 0; i < strlen(s); i++)
		if (((s[i] >= 65) && (s[i] <= 90)) || ((s[i]>= 97) && (s[i] <= 122)))
			c += 1;
	cout << c << endl;
	return 0;
}
Задание 2)Дан файл, содержащий русский текст, размер текста не превышает 10 К байт. Найти в тексте N
(N ≤ 100) самых длинных слов, содержащих заданную букву и не содержащих другую
заданную букву. Записать найденные слова в текстовый файл в порядке не возрастания длины.
Все найденные слова должны быть разными. Число N вводить из файла.
#include <iostream>
#include <fstream>
#define N 100
int main()
{
	std::ifstream in ("input.txt");
	if (!in)
	{
		std::cout << "Can't Open file";
		return -1;
	}
	std::ofstream out("output.txt");
	char s[100] = "Hello";
	std::string a[N];
	bool flag ;
	int n,k = 0;
	in >> n;
	while (!in.eof())
	{
		flag = false;
		in >> s;

		for (int i = 0; i < strlen(s); i++)
		{
			if (s[i] == 'h') { flag = false; break; }
			if (s[i] == 'i') flag = true;
		}
		if (flag)
			if (k < n)
			{
				a[k] = s;
				k += 1;
				// сортирую массив
				for (int i = k; i > 0; i--)
				{	std::string t = a[i];
					for (int j = 0; j < i; j++)
					{
						std::string d = a[j];
						if (((t.length()) > (d.length())) )
						{

							a[i] = a[j];
							a[j] = t;
							t = a[i];
						}
					}
				}
			}
			if (k >= n)
			{
				std::string q = a[n-1];
				if ((strlen(s)) > (q.length()))
				{
					a[n - 1] = s;
					for (int i = n - 1; i > 0; i--)
					{
						std::string t = a[i];
						for (int j = 0; j < i; j++)
						{
							std::string d = a[j];
							if (((t.length()) > (d.length())))
							{
								a[i] = a[j];
								a[j] = t;
								t = a[i];
							}
						}
					}
				}
			}
	}
	for (int i = 0; i < n; i++)
		out << a[i] << " ";

	return 0;
}
*/
/* Лекция 6
#include <iostream>
#define N  1000
// функция определения простоты числа
// тип данных, название, аргументы (может и не быть)
// передача по значению
bool isPrime(int x) // - Заголовок, сигнатура
{
	// тело цикла
	if (x < 2)
		// возврат результата
		return false;
	for (int d = 2; d <= sqrt(x); d++)
		if (x % d == 0)
			return false;
	return true;
}
// функция обмен двух переменных
// & - передача по ссылке
// void - функция ничего не возвращает (процедура)
void swap(int& a, int& b)
{
	int tmp = a;
	a = b;
	b = tmp;
}

// функция сортировки
// массив передан по ссылке
void sort(int n, int mas[N])
{
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (mas[i] < mas[j])
				swap(mas[i], mas[j]);
}
// Поиск суммы цифр числа
int sumOfDigits(int x)
{
	int sum = 0;
	while (x > 0)
	{
		sum += x % 10;
			x /= 10;
	}
	return sum;
}

void Read(int& n, int mas[1000])
{
	std::cin >> n;
	for (int i = 0; i < n; i++)
		std::cin >> mas[i];
}

bool ConsistsPrime(int n, int mas[1000])
{
	for (int i = 0; i < n; i++)
		if (isPrime(mas[i]))
			return true;
	return false;
}

void Write(int n, int mas[1000])
{
	for (int i = 0; i < n; i++)
		std::cout << mas[i] << " ";
}
int main()
{
	// Дана последовательность, если есть простое число, то упорядочить по возр.
	int n;
	int mas[1000];
	Read(n, mas);
	if (ConsistsPrime(n, mas))
		sort(n, mas);
	Write(n, mas);
	return 0;
}
*/
/* Лаба номер 6
Дана целочисленная матрица {Aij}i=1...n;j=1..n , n<=100. Если все диагональные элементы матрицы являются
наибольшими элементами своих строк, заменить элементы матрицы, содержащие цифру 0,
на произведение диагональных элементов.
#include <iostream>
#define N 100
#define M 100
bool Count(int n)
{
	int a[10], c;
	for (int i = 0; i < 10; i++) a[i] = 0;
	if ((n >= 0) && (n <= 9)) a[n] = 1;
	else
	{
		while (n > 0)
		{
			c = n % 10;
			a[c] += 1;
			n /= 10;
		}
	}

	if (a[0]) return true;
	return false;
}
void Read(int& n, int a[N][M])
{
	std::cin >> n;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++) std::cin >> a[i][j];
}
bool Check(int n, int a[N][M])
{
	int max, counter;
	for (int i = 0; i < n; i++)
	{
		counter = 0;
		max = INT_MIN;
		for (int j = 0; j < n; j++)
		{
			if (a[i][j] == max) { counter += 1; continue; }
			if (a[i][j] > max) { max = a[i][j]; counter = 1; }
		}
		if ((a[i][i] == max) && (counter == 1)) continue;
		else return false;
	}
	return true;
}
void Replace(int n, int a[N][M])
{
	int pr = 1;
	for (int i = 0; i < n; i++)
		pr *= a[i][i];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (Count(a[i][j])) a[i][j] = pr;
}
void Write(int n, int a[N][N])
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			std::cout << a[i][j] << " ";
		std::cout << std::endl;
	}
}
int main()
{
	int n;
	int a[N][M];
	Read(n, a);
	if (Check(n, a))
		Replace(n, a);
	Write(n, a);
	return 0;
}
*/
/* Лекция 8
#include <iostream>
void Change(int&n)
{
	n = 1234;
}
void Test()
{

	int n;
	std:: cin >> n;
	 // Создание динамического массива
	int *mas = new int[n];
	for (int i = 0; i < n; i++)
		std::cin>>mas[i];
	int sum=0;
	for (int i = 0; i < n; i++)
		sum += mas[i];
	std::cout << sum << std::endl;
	// Поработали и все- удаляем дин. массив
	delete[] mas;

	// двумерный массив
	int n = 3;
	int m = 3;
	int ** table = new int*[n];
	for (int i = 0; i < n; i++)
		table[i] = new int[m];
	table[0][0] = 10;

	for (int i = 0; i < n; i++)
		delete[] table[i];
	delete[] table;

	int mas[10] = {1,2,3,4,5,6,7,8,9,10};
	std::cout << mas[4] << std::endl;
	std::cout << *(mas + 4) << std::endl; // (mas+4) - указатель на 5-ый элемент и разыменовываем
	std::cout << mas << std::endl; // вывод адрес первого элемента
	std::cout << (mas+1) << std::endl; // - Перейти к адресу следующего элемента
}
// Передача массива в функцию
void sort(int** mas)
{

}
bool Func()
{
	int *mas = new int[1000];
	int x;
	std::cin >> x;
	if (x > 10)
	{
		delete[] mas; // программма упала
		return false;
	}

		delete[] mas;
		return false;

}
void Task()
{

	// 3 различных типа элементов. Каждого из типа может быть до 1 млн. значений
	// Суммарно все 3 элементов может быть не более 1 млн.
	int n,m,k;
	int *mas1 = new int[n]; // если 1 млн. - 4 мб.
	double *mas2 = new double[m];// если 1 млн. - 8 мб.
	char *mas3 = new char[k]; // если ввели ровно миллион - израсходовали 1 мб.
	delete[] mas1;
	delete[] mas2;
	delete[] mas3;

}
int main()
{
	// Ссылки
	int n = 1000;
	std:: cout << n << std::endl;
	// 1. Передача параметров в функции
	std:: cout << n << std::endl;
	Change(n);
	std:: cout << n << std:: endl;
	// 2. Псевдоним
	int SumOfDigitalOfPrimeNumber = 34;
	int &sum = SumOfDigitalOfPrimeNumber;
	std:: cout << SumOfDigitalOfPrimeNumber << std::endl;
	std:: cout << sum << std::endl;
	// Указатель - это переменная, которая хранит адрес другой переменной
	int *pointer = &n;
	std:: cout << pointer << std::endl;
	std:: cout << *pointer << std::endl;
	std::cout << sizeof(*pointer) << std::endl;
	// Область памяти - стек
	// 1. Фиксированный размер
	// 2. Быстрота
	// 3. Контроль со стороны компилятора
	int x;
	int mas[1000]; //разместить переменную "на стеке"

	// Область памяти - куча(Heap = хип)
	// 1. Произвольный размер
	// 2. Чуть медленее за счет выделения памяти
	// 3. Отсутствие контроля
	int *y = new int;
	*y = 10;
	*y++;
	std::cout << *y << std::endl;
	//delete y;
	// Memery Leak = утечка памяти
	//Test();
	// Указатель, котоырй никуда не указывает
	int *p = nullptr;
	if (p != nullptr)
	{
		delete p;
	}
	std:: cout << p << std::endl;
	return 0;
}
*/
/* Лаба 8
  1)Ввести последовательность натуральных чисел {Aj}j=1...n (n<=1000). Упорядочить последовательность по
  неубыванию суммы цифр числа, числа с одинаковыми суммами цифр дополнительно упорядочить по неубыванию наибольшей
  цифры числа, числа с одинаковыми суммами цифр и одинаковыми наибольшими цифрами дополнительно
  упорядочить по неубыванию самого числа. Сделать через дин. массив
#include <iostream>
#include <fstream>
#define N 1000
int Maxim(int a)
{
	if (a >= 0 && a<= 9) return a;
	else
	{
		int maxim = 0;
		while (a > 0)
		{
			(a % 10) > maxim? maxim = a % 10 : maxim;
			a /= 10;
		}
		return maxim;
	}
}
void Swaper( int &a, int &b)
{
	int tmp = b;
	b = a;
	a = tmp;
}

int Summa(int n)
{
	int a = 0;
	while (n > 0)
	{
		a += n % 10;
		n /= 10;
	}
	return a;
}
void Sort(int n ,int mas[N])
{
	for (int i = 0; i < n - 1;i++)
		for (int j = i + 1; j < n ; j++)
		{
			if ((Summa(mas[i])) > (Summa(mas[j]))) Swaper(mas[i], mas[j]);
			else if (((Summa(mas[i])) ==(Summa(mas[j])) ) && (Maxim(mas[i]) > Maxim(mas[j])) ) Swaper(mas[i],mas[j]);
			else if (((Summa(mas[i])) == (Summa(mas[j])) ) && (Maxim(mas[i]) == Maxim(mas[j])) && mas[i] > mas[j]) Swaper(mas[i],mas[j]);
		}
}
void Write(int n, int mas[N])
{
	for (int i = 0; i < n; i++) std::cout << mas[i] << std::endl;
}

int main()
{

	std::ifstream in("input.txt");
	if (!in)
	{std::cout << "Can't Open File"; return -1;}
	std::ofstream out("output.txt");
	int n;
	in >> n;
	int *mas = new int[N];
	for (int i = 0; i < n; i++) in >> mas[i];
	Sort(n,mas);
	Write(n,mas);
	delete[] mas;
	return 0;
}

2)Дана целочисленная матрица {Aij}i=1..n,j=1..m (n,m<=100). Найти строку с наибольшей суммой элементов
 и увеличить все элементы этой строки на 1.(сделать через дин. массив)
 #include <iostream>
#include <fstream>
#define N 100
int main()
{
	std::ifstream in("input.txt");
	if(!in)
	{std::cout <<"Can't Open File"; return -1;}
	int x,y,number,maxim=INT_MIN,summa;
	in >> x >> y;
	int** mas = new int* [x];
	for (int i = 0; i < x ; i++)
		mas[i] = new int[y];
	for (int i = 0; i < x; i++)
	{
		summa = 0;
		for (int j = 0; j < y; j++)
		{
			in >> mas[i][j];
			summa += mas[i][j];
		}
		if (summa > maxim) {maxim = summa; number = i;}
	}
	for (int j = 0; j < y; j++) mas[number][j] += 1;
	for (int i = 0; i < x;i++)
	{
		for (int j = 0; j < y; j++) std:: cout << mas[i][j] <<" ";
		std::cout <<std::endl;
	}
	for (int i = 0; i < x;i++)
		delete[] mas[i];
	delete[] mas;


	return 0;
}
 */

